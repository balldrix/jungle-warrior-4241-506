﻿// HealthManager.cs
// Christopher Ball 2016
// Script for managing player ammo

using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HealthManager : MonoBehaviour
{
    // starting health
    public const int STARTING_HEALTH = 100;

    // static health
    public static int health;

    // hud text
    Text text;

    // call on awake
    void Awake()
    {
        // get text component
        text = GetComponent<Text>();

        // set default health ammount
        health = STARTING_HEALTH;
    }

    // Update is called once per frame
    void Update()
    {
        // update text to health value
        text.text = health.ToString();

        // if health is zero then game over
        if(health < 0)
        {
            health = 0;
            GameManager.gameOver = true;
            
            // play game over sound
            AudioManager.instance.GameOver();
        }
    }
}
