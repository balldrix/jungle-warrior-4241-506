﻿// AudioManager.cs
// Christopher Ball 2016
// controls all game audio

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AudioManager : MonoBehaviour
{
    // reference to audio manager
    public static AudioManager instance;

    // sound clips
    public AudioClip gameOverSound;
    public AudioClip victorySound;
    public AudioClip newHighScoreSound;

    // list of music
    public List<AudioClip> music;
    
    private AudioSource source;

    private int currentTrack = 0;

    // Use this for initialization
    void Awake()
    {
        if (instance == null)
        {
            // don't destroy this 
            DontDestroyOnLoad(gameObject);

            // set ref to this audio manager
            instance = this;

            // get references to audio sources
            source = GetComponent<AudioSource>();
            source = GetComponent<AudioSource>();
            source.clip = music[currentTrack];
            source.Play();
        }
        else if (instance != this)
        {
            // destroy object
            Destroy(gameObject);
        }
    }

    // play game over sound
    public void GameOver()
    {
        // play game over sound
        source.PlayOneShot(gameOverSound, 2.0f);
    }

    // play new high score sounds
    public void NewHighScore()
    {
        // play high score sound
        source.PlayOneShot(newHighScoreSound, 2.0f);
    }

    // play victory sound
    public void Victory()
    {
        // play sound
        source.PlayOneShot(victorySound, 2.0f);
    }

    // changes music track
    public void SkipTrack()
    {
        source.Stop();

        // change audio track
        if (currentTrack < music.Count - 1)
        {
            currentTrack++;
        }
        else
        {
            currentTrack = 0;
        }
        source.clip = music[currentTrack];
        source.Play();
    }
}
