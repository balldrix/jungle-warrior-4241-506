﻿// TimeManager.cs
// Christopher Ball 2016
// Script to keep and display timer

using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TimeManager : MonoBehaviour
{
    // starting time
    public const float START_TIME = 0.0f;

    // hud text
    Text text;

    // time keeper
    public float time;

    // Use this for initialization
    void Awake()
    {
        // set time to zero
        time = 0.0f;

        // get text component
        text = GetComponent<Text>();

    }

    // Update is called once per frame
    void Update()
    {
        // update time
        time += Time.deltaTime;

        // set minutes
        float minutes = time / 60;

        // set seconds
        float seconds = time % 60;

        // set milliseconds
        float milliseconds = (time * 100) % 100;

        // display text as 00:00:00
        text.text = string.Format("{0:00} : {1:00} : {2:00}", minutes, seconds, milliseconds);
    }
}
