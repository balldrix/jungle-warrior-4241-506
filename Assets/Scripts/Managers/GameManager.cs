﻿// GameManager.cs
// Christopher Ball 2016
// manages player score

using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameManager : MonoBehaviour
{
    // points awarded for a kill
    public const int KILL_POINTS = 10;

    // max scenes
    public const int MAX_SCENES = 4;

    // public static ref to game manager
    public static GameManager instance;

    // tracks game over state
    public static bool gameOver;

    // tracks victory state
    public static bool victory;

    // tracks paused state
    public static bool paused;

    // player score
    public static int playerScore;

    // high score
    public static int highScore;

    // call on awake
    void Awake()
    {
        // if there is no score manager ref
        if(instance == null)
        {
            // dont destroy on load
            DontDestroyOnLoad(gameObject);

            // set ref to this score manager
            instance = this;

            // set game over
            gameOver = false;

            // set victory
            victory = false;

            // set un paused
            paused = false;

            // set score to 0
            playerScore = 0;

            // load high score
            highScore = PlayerPrefs.GetInt("highscore");
        }
        else if(instance != this)
        {
            // destroy this object
            Destroy(gameObject);
        }
    }

    // save high score
    public bool SaveHighScore()
    {
        // if player score is higher than high score, save it
        if (playerScore > highScore)
        {
            // play high score sound
            AudioManager.instance.NewHighScore();

            // save high score
            PlayerPrefs.SetInt("highScore", playerScore);

            return true;
        }
        else
        {
            return false;
        }
    }
}
