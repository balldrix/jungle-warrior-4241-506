﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HUDTargetsManager : MonoBehaviour
{
    // score text
    Text text;

    // num targets left
    public static int targets = 0;

    // call on awake
    void Awake()
    {
        // get text component
        text = GetComponent<Text>();

        targets = 0;
    }

    // Update is called once per frame
    void Update()
    {
        text.text = "Targets: " + targets;
    }
}
