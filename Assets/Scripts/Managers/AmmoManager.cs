﻿// AmmoManager.cs
// Christopher Ball 2016
// Script for managing player ammo

using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AmmoManager : MonoBehaviour
{   
    // starting ammo
    private const int STARTING_AMMO = 30;

    // max ammo
    public const int MAX_AMMO = 200;

    // static ammo
    public static int ammo;

    // hud text
    Text text;

    // call on awake
    void Awake()
    {           
        // get text component
        text = GetComponent<Text>();

        // set default ammo ammount
        ammo = STARTING_AMMO;
    }

    // Update is called once per frame
    void Update()
    {
        // update text to ammo value
        text.text = ammo.ToString();
    }
}