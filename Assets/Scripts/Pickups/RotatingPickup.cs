﻿// RotatingPickup.cs
// Christopher Ball 2016
// rotates pickup item around y axis

using UnityEngine;
using System.Collections;

public class RotatingPickup : MonoBehaviour
{
    // rotation speed
    public float rotationSpeed = 50;

    // Update is called once per frame
    void Update()
    {
        // rotate the item around the y axis
        transform.Rotate(0.0f, rotationSpeed * Time.deltaTime, 0.0f);
    }
}
