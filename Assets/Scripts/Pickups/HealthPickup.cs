﻿// HealthPickup.cs
// Christopher Ball 2016
// Adds health to player when pickup is triggered

using UnityEngine;
using System.Collections;

public class HealthPickup : MonoBehaviour
{
    // health pickup amount
    public const int HEALTH_PICKUP = 50;

    // collision particle system
    public GameObject particle;

    // gun shot sounds
    public AudioClip pickupSound;

    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            // make sure health is not already at max
            if (HealthManager.health < HealthManager.STARTING_HEALTH)
            {
                // play pickup sound
                AudioSource.PlayClipAtPoint(pickupSound, transform.position, 2.0f);

                // get enemy health
                HealthManager.health += HEALTH_PICKUP;

                // make sure health does not go over 100
                if (HealthManager.health > HealthManager.STARTING_HEALTH)
                {
                    HealthManager.health = HealthManager.STARTING_HEALTH;
                }

                // spawn particle effect
                GameObject effect = Instantiate(particle) as GameObject;

                effect.transform.position = gameObject.transform.position;

                // destroy particle effect in 3 seconds
                Destroy(effect, 2.0f);

                // destroy health pickup
                Destroy(gameObject);
            }
        }
    }
}
