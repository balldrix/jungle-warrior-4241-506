﻿// AmmoPickup.cs
// Christopher Ball 2016
// adds ammo to player when pickup is triggered

using UnityEngine;
using System.Collections;

public class AmmoPickup : MonoBehaviour
{
    // health pickup amount
    public const int AMMO_PICKUP = 30;

    // collision particle system
    public GameObject particle;

    // gun shot sounds
    public AudioClip pickupSound;

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            // make sure ammo is not alread at max
            if (AmmoManager.ammo < AmmoManager.MAX_AMMO)
            {
                // play pickup sound
                AudioSource.PlayClipAtPoint(pickupSound, transform.position, 2.0f);

                // get enemy health
                AmmoManager.ammo += AMMO_PICKUP;

                // make sure health does not go over 100
                if (AmmoManager.ammo > AmmoManager.MAX_AMMO)
                {
                    AmmoManager.ammo = AmmoManager.MAX_AMMO;
                }

                // spawn particle effect
                GameObject effect = Instantiate(particle) as GameObject;

                effect.transform.position = gameObject.transform.position;

                // destroy particle effect in 3 seconds
                Destroy(effect, 2.0f);

                // destroy health pickup
                Destroy(gameObject);
            }
        }
    }
}
