﻿// VictoryMenu.cs
// Christopher Ball 2016
// manages game over display

using UnityEngine;
using UnityEngine.SceneManagement;
using UnityStandardAssets.Characters.FirstPerson;

public class VictoryMenu : MonoBehaviour
{
    // reference to the menu
    public Canvas menu;

    // time grades
    public float goldTime = 15.0f;
    public float silverTime = 25.0f;
    public float bronzeTime = 40.0f;

    // scores based on times
    public int goldScore = 5000;
    public int silverScore = 2000;
    public int bronzeScore = 1000;

    // reference to time manager
    private TimeManager timeManager;
    
    // Use this for initialization
    void Start()
    {
        // get references
        menu = GetComponent<Canvas>();
        timeManager = GameObject.Find("TimeText").GetComponent<TimeManager>();

        // turn menu off
        menu.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(GameManager.victory)
        {
            Victory();
        }
    }

    // game over script
    public void Victory()
    {
        // save high score
        if(!GameManager.instance.SaveHighScore())
        {
            // play victory sound
            AudioManager.instance.Victory();
        }

        if(timeManager.time < goldTime)
        {
            GameManager.playerScore += goldScore;
        }
        else if(timeManager.time < silverTime)
        {
            GameManager.playerScore += silverScore;
        }
        else if(timeManager.time < bronzeTime)
        {
            GameManager.playerScore += bronzeScore;
        }

        // pause time scale
        Time.timeScale = 0.0f;

        // turn on mouse cursor
        Cursor.visible = true;

        // unlock cursor
        Cursor.lockState = CursorLockMode.None;

        // turn menu on
        menu.enabled = true;

        // turn off game victory loop
        GameManager.victory = false;
    }

    // play again
    public void Play()
    {
        // resume normal time scale
        Time.timeScale = 1.0f;

        // turn off menu canvas
        menu.enabled = false;

        // game over is false
        GameManager.gameOver = false;

        // turn off mouse cursor
        Cursor.visible = false;

        // unlock cursor
        Cursor.lockState = CursorLockMode.Locked;

        // get scene index
        int scene = SceneManager.GetActiveScene().buildIndex;

        // if scene is not last
        if(scene < GameManager.MAX_SCENES - 1)
        {
            // load next scene
            SceneManager.LoadScene(scene + 1, LoadSceneMode.Single);
        }
        else
        {
            // load first scene
            SceneManager.LoadScene(1);
        }
    }

    // back to menu
    public void BackToMenu()
    {
        // enable normal time scale
        Time.timeScale = 1.0f;

        // turn off mouse cursor
        Cursor.visible = true;

        // set game over state
        GameManager.gameOver = false;

        // load manu menu scene
        SceneManager.LoadScene(0);
    }

    // exit
    public void Exit()
    {
        Application.Quit();
    }
}
