﻿// TitleMenuManager.cs
// Christopher Ball 2016
// manages main menu options

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class TitleMenuManager : MonoBehaviour
{
    // refence canvas's
    public Canvas mainMenu;
    public Canvas levelSelect; 
    
    // Use this for initialization
    void Start()
    {
        // get references
        mainMenu = mainMenu.gameObject.GetComponent<Canvas>();
        levelSelect = levelSelect.gameObject.GetComponent<Canvas>();

        // turn on main menu
        ReturnToMenu();
    }

    // calls when play is clicked
    public void Play(int index)
    {
        // load scene by index
        SceneManager.LoadScene(index);
    }

    // opens level select menu
    public void LevelSelect()
    {
        // enable all level select buttons
        levelSelect.enabled = true;

        // diable main menu buttons
        mainMenu.enabled = false;
    }

    // returns to main menu
    public void ReturnToMenu()
    {
        // disable all level select buttons
        levelSelect.enabled = false;

        // enable main menu buttons
        mainMenu.enabled = true;
    }

    // exit button
    public void ExitGame()
    {
        Application.Quit();
    }
}
