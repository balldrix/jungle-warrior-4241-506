﻿// ScoreText.cs
// Christopher Ball 2016
// displays score text

using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreText : MonoBehaviour
{
    // hud text
    Text text;

    // Use this for initialization
    void Start()
    {
        // get text component
        text = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        // set score text
        text.text = "Score: " + GameManager.playerScore;
    }
}
