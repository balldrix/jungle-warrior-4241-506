﻿// GameMenu.cs
// Christopher Ball 2016
// manages in game menu

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityStandardAssets.Characters.FirstPerson;

public class GameMenu : MonoBehaviour
{
    // reference to menu
    public Canvas menu;

    // Use this for initialization
    void Awake()
    {
        // get references
        menu = GetComponent<Canvas>();

        // turn menu off
        menu.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        // if escape is pressed 
        if (Input.GetKeyDown("escape"))
        {
            if(GameManager.paused)
            {
                Resume(); // resume game
            }
            else if(!GameManager.paused)
            {
                Pause(); // pause game
            }
        }
    }

    // resume game
    public void Resume()
    {
        // set paused bool
        GameManager.paused = false;

        // resume normal time scale
        Time.timeScale = 1.0f;

        // turn off menu canvas
        menu.enabled = false;

        // turn off mouse cursor
        Cursor.visible = false;

        // lock cursor
        Cursor.lockState = CursorLockMode.Locked;
    }

    // pause game
    public void Pause()
    {        
        // set paused bool
        GameManager.paused = true;

        // pause time scale
        Time.timeScale = 0.0f;

        // turn on menu canvas
        menu.enabled = true;

        // turn on mouse cursor
        Cursor.visible = true;

        // unlock cursor
        Cursor.lockState = CursorLockMode.None;
    }

    // exit button
    public void ExitGame()
    {
        SceneManager.LoadScene(0);
    }

    // skip track
    public void SkipTrack()
    {
        AudioManager.instance.SkipTrack();
    }
}
