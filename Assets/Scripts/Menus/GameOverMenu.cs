﻿// GameOverMenu.cs
// Christopher Ball 2016
// manages game over display

using UnityEngine;
using UnityEngine.SceneManagement;
using UnityStandardAssets.Characters.FirstPerson;

public class GameOverMenu : MonoBehaviour
{
    // reference to the menu
    public Canvas menu;

    // Use this for initialization
    void Start()
    {
        // get references
        menu = GetComponent<Canvas>();

        // turn menu off
        menu.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(GameManager.gameOver)
        {
            GameOver();
            GameManager.gameOver = false;
        }
    }

    // game over script
    public void GameOver()
    {
        // pause time scale
        Time.timeScale = 0.0f;

        // turn on mouse cursor
        Cursor.visible = true;

        // unlock cursor
        Cursor.lockState = CursorLockMode.None;

        // turn menu on
        menu.enabled = true;

        // save high score
        GameManager.instance.SaveHighScore();
    }

    // play again
    public void PlayAgain()
    {
        // resume normal time scale
        Time.timeScale = 1.0f;

        // turn off menu canvas
        menu.enabled = false;

        // game over is false
        GameManager.gameOver = false;

        // turn off mouse cursor
        Cursor.visible = false;

        // unlock cursor
        Cursor.lockState = CursorLockMode.Locked;

        // re load current scene
        int scene = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(scene, LoadSceneMode.Single);
    }

    // back to menu
    public void BackToMenu()
    {
        // enable normal time scale
        Time.timeScale = 1.0f;

        // turn off mouse cursor
        Cursor.visible = true;

        // set game over state
        GameManager.gameOver = false;

        // load menu menu scene
        SceneManager.LoadScene(0);
    }

    // exit
    public void Exit()
    {
        Application.Quit();
    }
}
