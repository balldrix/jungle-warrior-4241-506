﻿// Bullet.cs
// Christopher Ball 2016
// Manages bullet prefab

using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour
{
    // constants
    public const float BULLET_LIFE = 3.0f; // bullet life time
    public int DAMAGE = 18; // bullet damage

    // gun shot sounds
    public AudioClip hitSound;

    void Awake()
    {
        // destroy bullet after liftime is up
        Destroy(gameObject, BULLET_LIFE);
    }

    void OnTriggerEnter(Collider other)
    {
        // show other collision object name in console
        Debug.Log("Bullet has collision with " + other.gameObject.name);

        // make gun shot sound
        AudioSource.PlayClipAtPoint(hitSound, transform.position, 2.0f);

        // set bullet to inactive
        gameObject.SetActive(false);

        // check what bullet collided with
        if (other.tag == "Player")
        {
            // reduce player health
            HealthManager.health -= DAMAGE;

            // debug text
            Debug.Log("Player hurt, new health is " + HealthManager.health);
        }

        if(other.tag == "Enemy")
        {
            // reduce enemy health
            EnemyHealth enemyhealth = other.GetComponent<EnemyHealth>();

            // check enemy health exists
            if(enemyhealth != null)
            {
                enemyhealth.ReduceHealth(DAMAGE);
            }
        }
    }
}
