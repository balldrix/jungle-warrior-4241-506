﻿// EnemyAI.cs
// Christopher Ball 2016
// controls enemy AI states with waypoint patrol


using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyAI : MonoBehaviour
{
    // constants
    private const float FIRING_RANGE = 30.0f; // enemy firing range
    private const float VIEW_RANGE = 60.0f; // viewing range
    private const float VIEW_ANGLE = 45.0f; // view angle    

    // enemy gun
    public Transform enemyGun;

    // player target
    public Transform player;

    // player distance
    private float playerDistance;

    // current waypoint
    public int currentWaypoint;

    // list of waypoints
    public List<Transform> waypoints;

    // AI states enum
    public enum AIStates
    {
        PATROL,
        FOLLOW,
        SHOOT
    }

    // current AI state
    private AIStates state;

    // navmesh agent
    NavMeshAgent agent;

    // Use this for initialization
    void Start()
    {
        // get ref to nav agent
        agent = GetComponent<NavMeshAgent>();

        // set state to patrol
        state = AIStates.PATROL;

        // set waypoint to first in index
        currentWaypoint = 0;

        PickNextWaypoint();
    }

    // Update is called once per frame
    void Update()
    {
        // switch using AI states
        switch (state)
        {
            case AIStates.PATROL: // enemy patrol state
                state = Patrol();
                break;
            case AIStates.FOLLOW: // enemy follow state
                state = Follow();
                break;
            case AIStates.SHOOT: // enemy shooting state
                state = Shoot();
                break;
        }
    }

    void PickNextWaypoint()
    {
        // if there are more than 0 waypoints
        if (waypoints.Count > 0)
        {
            // set AI movement to waypoint position
            agent.destination = waypoints[currentWaypoint].position;

            // circular waypoints
            if (currentWaypoint < waypoints.Count - 1)
            {
                // increase waypoint index
                currentWaypoint++;
            }
            else
            {
                // reset index to 0
                currentWaypoint = 0;
            }
        }
    }

    AIStates Patrol()
    {
        // debug text
        //Debug.Log(gameObject.name + "'s in " + state + " state");

        // if enemy is close to current waypoint then change to the next
        if (agent.remainingDistance <= 0.1f)
        {
            PickNextWaypoint();
        }

        // if in sight and view range
        if (IsInSight() && IsInViewRange())
        {
            // debug text
            Debug.Log(gameObject.name + " can see player");

            // player is in view so change state
            return AIStates.FOLLOW;
        }

        // no state change so return patrol
        return AIStates.PATROL;
    }

    AIStates Follow()
    {
        // debug text
        //Debug.Log(gameObject.name + "'s in " + state + " state");

        // follow player
        agent.destination = player.transform.position;
       
        // if out of view range
        if(!IsInViewRange())
        {
            // go to next waypoint
            PickNextWaypoint();

            // return patrol state
            return AIStates.PATROL;
        }

        // check if enemy is in firing range
        playerDistance = Vector3.Distance(transform.position, player.position);

        if (playerDistance < FIRING_RANGE)
        {
            // change to shoot state
            return AIStates.SHOOT;
        }

        // continue to follow
        return AIStates.FOLLOW;
    }

    AIStates Shoot()
    {
        // debug text
        //Debug.Log(gameObject.name + "'s in " + state + " state");

        // stop nav agent while shooting
        agent.Stop();

        // turn to face player
        transform.LookAt(player);

        // turn gun towards player
        enemyGun.LookAt(player);

        // shoot
        EnemyShooting enemyShooting = GetComponent<EnemyShooting>();
        enemyShooting.FireGun(player);

        // if player is out of range switch back to patrol
        float playerDistance = Vector3.Distance(player.position, transform.position);
        if (playerDistance > FIRING_RANGE)
        {
            // debug text
            //Debug.Log(gameObject.name + " is out of range.");

            // resume navagent
            agent.Resume();

            // return patrol state
            return AIStates.PATROL;
        }

        // return shoot state
        return AIStates.SHOOT;
    }

    // checks to see if player is within enemy viewing cone
    bool IsInSight()
    {
        // get angle to player
        float playerAngle = Vector3.Angle(this.transform.forward, player.transform.position);

        if(playerAngle < VIEW_ANGLE)
        {
            if (Physics.Raycast(this.transform.position, player.transform.position - this.transform.position))
            {
                // player is in view so return true
                return true;
            }
        }

        // enemy player is out of sight
        return false;
    }
    // checks if in view distance
    bool IsInViewRange()
    {
        // get distance from player
        playerDistance = Vector3.Distance(transform.position, player.position);

        // if within distance
        if (playerDistance < VIEW_RANGE)
        {
            // true
            return true;
        }
        else
        {
            // false
            return false;
        }
    }
}
