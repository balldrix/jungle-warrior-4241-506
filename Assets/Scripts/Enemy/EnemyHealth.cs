﻿// EnemyHealth.cs
// Christopher Ball 2016
// stores and updates enemy health

using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
    private const int STARTING_HEALTH = 50; // enemy starting health    

    // enemy health
    private int health;

    // collision particle system
    public GameObject particle;

    // gun shot sounds
    public AudioClip deathSound;

    // Use this for initialization
    void Start()
    {
        // starting health
        health = STARTING_HEALTH;

        // increase num targets in hud
        HUDTargetsManager.targets++;
    }

    // call this when enemy is hit
    public void ReduceHealth(int damage)
    {
        // reduce health
        health -= damage;

        // if health is reduced to 0
        if(health <= 0)
        { 
            // play dead sound
            AudioSource.PlayClipAtPoint(deathSound, transform.position, 2.0f);

            // spawn particle effect
            GameObject effect = Instantiate(particle) as GameObject;

            effect.transform.position = gameObject.transform.position;

            // destroy particle effect in 3 seconds
            Destroy(effect, 2.0f);

            // add score
            GameManager.playerScore += GameManager.KILL_POINTS;

            // change targets left on hud
            HUDTargetsManager.targets--;    

            // remove enemy
            Destroy(gameObject);
        }
    }
}
