﻿// EnemyShooting.cs
// Christopher Ball 2016
// controls enemy shooting rate and bullet spawn

using UnityEngine;
using System.Collections;

public class EnemyShooting : MonoBehaviour
{
    // constants
    private const float RATE_OF_FIRE = 0.6f; // shooting rate of fire

    // enemy bullet
    public GameObject bullet;

    // bullet spawn location
    public Transform bulletSpawn;

    // gun shot sounds
    public AudioClip gunshotSound;

    // sound source
    private AudioSource source;

    // bullet force
    public float force = 1000.0f;

    // firing timer
    private float fireRateTimer = 0.0f;

    // Use this for initialization
    void Awake()
    {
        // get reference to audio source
        source = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        // update timer
        fireRateTimer += Time.deltaTime;

        // debug text
        //Debug.Log(gameObject.name + " fire rate is " + fireRateTimer);
    }

    public void FireGun(Transform target)
    {
        // only fire at rate of fire
        if (fireRateTimer > RATE_OF_FIRE)
        {
            // debug text
            Debug.Log(gameObject.name + " shoots at " + target.name);

            // make gun shot sound
            source.PlayOneShot(gunshotSound, 1.0f);

            // reset fire rate
            fireRateTimer = 0.0f;

            // spawn bullet prefab
            GameObject bulletClone = Instantiate(bullet, bulletSpawn.position, bulletSpawn.rotation) as GameObject;

            // tag buller
            bulletClone.tag = "Enemy";

            // shoot bullet with add force at set force
            bulletClone.GetComponent<Rigidbody>().AddForce(bulletSpawn.forward * force);
        }
    }
}
