﻿// PlayerShooting.cs
// Christopher Ball 2016
// Script for shooting bullets

using UnityEngine;
using System.Collections;

public class PlayerShooting : MonoBehaviour
{
    // constants
    private const float RATE_OF_FIRE = 0.6f; // shooting rate of fire

    // bullet game object
    public GameObject bullet;

    // bullet spawn location
    public Transform bulletSpawn;

    // player camera 
    public GameObject playerCamera;

    // gun shot sounds
    public AudioClip gunshotSound;

    // sound source
    private AudioSource source;

    // bullet force
    public float force = 1000.0f;

    // fire timer
    private float fireRateTimer = 0.0f;

    // on start
    void Awake()
    {
        // get reference to audio source
        source = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        // check for fire1 button
        if(Input.GetButtonDown("Fire1") 
            && (fireRateTimer > RATE_OF_FIRE) 
            && !GameManager.paused 
            && !GameManager.victory
            && !GameManager.gameOver)
        {           
            if(AmmoManager.ammo > 0)
            {
                // shoot weapon
                Shoot();
            }

            // reset timer
            fireRateTimer = 0.0f;
        }

        // update firerate timer
        fireRateTimer += Time.deltaTime;
    }

    void Shoot()
    {
        // debug text
        Debug.Log(gameObject.name + " has fired gun.");

        // make gun shot sound
        source.PlayOneShot(gunshotSound, 1.0f);

        // reduce rounds in cartridge
        AmmoManager.ammo--;

        // spawn bullet prefab
        GameObject bulletClone = Instantiate(bullet, bulletSpawn.position, bulletSpawn.rotation) as GameObject;

        // tag bullet
        bulletClone.tag = "Player";

        // shoot bullet with add force at set force
        bulletClone.GetComponent<Rigidbody>().AddForce(bulletSpawn.transform.forward * force);
    }
}
