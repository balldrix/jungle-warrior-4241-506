﻿// PlayerExit.cs
// Christopher Ball 2016
// detects player collision to end level

using UnityEngine;
using System.Collections;

public class PlayerExit : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Finish")
        {
            // TODO play exit sound

            // trigger exit
            GameManager.victory = true;
        }
    }
}
